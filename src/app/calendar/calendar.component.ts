import { Component, OnInit, Input } from '@angular/core';
import { Calendar } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import resourceTimeGridPlugin from '@fullcalendar/resource-timegrid';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeline from '@fullcalendar/timeline';
declare var $: any;

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  @Input('#calendario') calendar: any;
  today: Date;
  currentHour: number;
  firstTimeCalendar: string;
  calendar1: HTMLElement;
  daily: Calendar;


  constructor() { }

  ngOnInit() {
    // console.log(this.calendar);
    const cal = document.getElementById('calendario');
    console.log(cal);

    this.today = new Date();
    this.currentHour = this.today.getHours();
    this.firstTimeCalendar = this.today.getHours() + ":00:00";

    this.configCalendar();

  }

  configCalendar() {
    this.calendar1 = document.getElementById("calendario");

    this.daily = new Calendar(this.calendar1, {
      schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
      plugins: [
        resourceTimeGridPlugin,
        dayGridPlugin,
        interactionPlugin,
        timeline
      ],
      defaultView: 'resourceTimeGridDay',
      nowIndicator: true,
      header: {
        left: '',
        center: 'title',
        right: ''
      },
      titleFormat: {
        month: '2-digit',
        year: 'numeric',
        day: '2-digit',
        weekday: 'long',
        separator: '/'
      },
      dayRender: (dayRenderInfo) => {
        // dayRenderInfo.el.style.backgroundColor = "#ffffff";
        // this.formatStylesCalendar();
      },
      resources: [
        { id: 'a', title: 'Room A' },
        { id: 'b', title: 'Room B' },
        { id: 'c', title: 'Room C' },
        { id: 'd', title: 'Room D' }
      ],
      events: 'https://fullcalendar.io/demo-events.json?with-resources=4&single-day'
    });

    this.daily.render();
  }


}
